var ctarget = "login.page_load";
var gateway = "gateway.html";

$(document).ready(function() {
	let form = call(ctarget, page_set);
});

function call(target, response_cb) {
	let context =
		{
			target: target
		};
	
	gateway_call(context, response_cb);
}

function gateway_call(context, response_cb) {
    let ctxret = null;
    
	let request = $.ajax({
		url: gateway,
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(context),
		success: function(data, status) {
			response_cb(data);
		},
		error: function(data, status) {
			response_cb(data);
		}});
}

function page_set(form) {
	$("#main").append(form.content);
	
	for (component of form.components) {
		switch (component.type) {
		case "control":
			$(component.name).on(
				component.event,
				function() {
					call(component.target, page_set);
				});
			break;
		}
	}
}
