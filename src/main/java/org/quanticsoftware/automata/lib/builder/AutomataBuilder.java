package org.quanticsoftware.automata.lib.builder;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.GeneralException;
import org.quanticsoftware.automata.core.target.Target;
import org.quanticsoftware.automata.runtime.AutomataRuntime;
import org.quanticsoftware.automata.runtime.Program;

public class AutomataBuilder {
	
	public static final Map<String, Program> execute(Context context) throws Exception {
		var sprograms = new HashMap<String, Program>();
		var runtime = new AutomataRuntime();
		
		for (var targetid : context.targets()) {
			var target = context.getTarget(targetid);
			
			for (var testid : target.tests())
				target.getTest(testid).seal();
			
			var cauldron = Cauldron.instance(context, target);
			cauldron.setMaxLevel(5);
	
			var ret = "fail";
			long range = 0;
				
			for (int i = 0; i < 2000; i++) {
				var ini = new Date().getTime();
				var programs = cauldron.execute();
				var fin = new Date().getTime();
				
				range += fin - ini;
				
				if (programs == null) {
					System.err.printf(
							"Couldn't generate program for target '%s'.\n",
							target.name());
					break;
				}
				
				try {
					for (var program : programs) {
						System.out.println(program.toString());
						
						var success = test(runtime, context, program, target);
						if (!success)
							continue;
						
						System.out.println("Success!");
						sprograms.put(targetid, program);
						
						throw new SuccessfulTestException();
					}
				} catch (SuccessfulTestException e) {
					ret = "success";
					break;
				}
			}
			
			System.out.println(Concatenate.execute(
					targetid,
					" time: ",
					range,
					" status: ",
					ret));
		}
		
		return sprograms;
	}
	
	private static final boolean test(
			AutomataRuntime runtime,
			Context context,
			Program program,
			Target target) throws Exception {
		var otype = target.getOutputType();
		
		for (var testid : target.tests()) {
			System.out.println(Concatenate.execute(
					"target: ",  target.name(),
					", test: ", testid));
			
			var test = target.getTest(testid);
			DataObject poutput = null;
			
			try {
				var input = new HashMap<String, DataObject>();
				target.input().forEach(k->input.put(k, test.input(k)));
				poutput = runtime.execute(context, program, input);
			} catch (GeneralException e) {
				var omessage = test.getFailMessage();
				if (omessage == null) {
					e.printStackTrace();
					return false;
				}
				
				var emessage = e.getMessage();
				System.out.println("calculated:");
				System.out.printf("exception %s.\n", emessage);
				System.out.println("test:");
				System.out.printf("exception %s.\n", omessage);
				System.out.println(" --- ");
				if (omessage.equals(emessage))
					continue;
				
				return false;
			} catch (Exception e) {
				System.out.printf("runtime error in: %s\n ", program.toString());
				e.printStackTrace();
				return false;
			}
			
			var toutput = test.output();
			System.out.println("calculated:");
			System.out.println(poutput.toString());
			
			var otest = test.getOutputTest();
			if (otest != null) {
				try {
					if (otest.test(poutput))
						continue;
				} catch (Exception e) {
					
				}
				
				return false;
			}
			
			System.out.println("test:");
			System.out.println(toutput.toString());
			System.out.println(" --- ");
			
			if (!test(otype, poutput, toutput))
				return false;
		}
		
		return true;
	}
	
	private static boolean test(DataType type, Object p, Object t) {
		if (!type.isPrimitive()) {
			if (t == p)
				return true;
			
			return (t != null) && t.toString().equals(p.toString());
		}
		
		var toutput = (DataObject)t;
		var poutput = (DataObject)p;
		
		for (var key : type.items())
			if (!test(type.get(key), poutput.get(key), toutput.get(key)))
				return false;
		
		return true;
	}
}

class SuccessfulTestException extends Exception {
	private static final long serialVersionUID = -88466559552659736L;
	
}
