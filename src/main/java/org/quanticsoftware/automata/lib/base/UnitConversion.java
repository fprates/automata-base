package org.quanticsoftware.automata.lib.base;

import java.util.HashMap;
import java.util.Map;

public class UnitConversion {
	private static Map<String, String> domainref;
	private static Map<String, UnitConversion> cmap;
	private String domain;
	private Map<String, UnitConversionRule> rules;
	
	static {
		domainref = new HashMap<>();
		cmap = new HashMap<>();
	}
	
	public UnitConversion(String domain) {
		this.domain = domain;
		rules = new HashMap<>();
	}
	
	public final double execute(String tunit, String sunit, double input) {
		var svalue = rules.get(sunit).convert(input);
		return svalue / (rules.get(tunit).convert(1));
	}
	
	public static final UnitConversion get(String unit) {
		return cmap.get(domainref.get(unit));
	}
	
	public static final UnitConversion instance(String domain) {
		return cmap.computeIfAbsent(
				domain,
				k->new UnitConversion(domain));
	}
	
	public static final boolean isSameDomain(String target, String source) {
		return domainref.get(target).equals(domainref.get(source));
	}
	
	public final void unit(String unit, UnitConversionRule rule) {
		rules.put(unit, rule);
		domainref.put(unit, domain);
	}
}
