package org.quanticsoftware.automata.lib.base;

import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.target.Target;

public class ProgramInputConfig {
	private DataContext datactx;
	
	public ProgramInputConfig(Target target, DataContext datactx) {
		this.datactx = datactx;
		
		for (var key : target.input())
			datactx.instance(target.input(key), key);
	}
	
	public final DataObject input(String name) {
		return datactx.get(name);
	}
	
}
