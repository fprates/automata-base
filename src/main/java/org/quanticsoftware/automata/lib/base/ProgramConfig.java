package org.quanticsoftware.automata.lib.base;

public interface ProgramConfig {

	public abstract void execute(ProgramDefinition pd);
	
}
