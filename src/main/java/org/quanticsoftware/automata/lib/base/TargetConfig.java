package org.quanticsoftware.automata.lib.base;

import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.target.Target;

public interface TargetConfig {

	public abstract void execute(Target target, Context context);
	
}