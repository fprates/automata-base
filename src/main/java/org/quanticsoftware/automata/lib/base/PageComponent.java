package org.quanticsoftware.automata.lib.base;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.DataType;

public class PageComponent {
	private DataType type, types;
	private PageComponent parent, root;
	private Map<String, PageComponent> components;
	private Set<String> actions;
	private String name;
	
	public PageComponent(
			String name,
			PageComponent root,
			PageComponent parent,
			DataType type,
			DataType types) {
		this.name = name;
		this.parent = parent;
		this.type = type;
		this.types = types;
		components = new LinkedHashMap<>();
		
		if (root != null)
			return;
		
		actions = new HashSet<>();
		this.root = this;
	}
	
	public final PageComponent button(String name) {
		root.actions.add(name);
		return instance(name, "button");
	}
	
	public final void buttonbar(String name, String... actions) {
		for (var action : actions)
			button(action);
	}
	
	public final PageComponent custom(String name) {
		return instance(name, "custom");
	}
	
	public final PageComponent dataform(String name, String type) {
		return instance(name, "dataform");
	}
	
	public final PageComponent get(String name) {
		return components.get(name);
	}
	
	private final PageComponent instance(String name, String typename) {
		type.add(types.get(typename), name);
		
		var component = new PageComponent(
				name,
				root,
				this,
				type.get(name),
				types);
		
		components.put(name, component);
		return component;
	}
	
	public final Set<String> items() {
		return components.keySet();
	}
	
	public final String name() {
		return name;
	}
	
	public final PageComponent parent() {
		return parent;
	}
	
	public final PageComponent section(String name) {
		return instance(name, "section");
	}
	
	public final PageComponent table(String name, String type) {
		return instance(name, "table");
	}
	
	public final PageComponent tab(String name) {
		return instance(name, "tab");
	}
	
	public final PageComponent tabs(String name) {
		return instance(name, "tabs");
	}
	
	@Override
	public final String toString() {
		return Concatenate.execute(name, "(", type.name(), ")");
	}
	
	public final DataType type() {
		return type;
	}
}
