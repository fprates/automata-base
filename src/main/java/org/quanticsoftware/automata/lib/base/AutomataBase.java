package org.quanticsoftware.automata.lib.base;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.cauldron.Cauldron;
import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataContext;
import org.quanticsoftware.automata.core.DataObject;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.TypeContext;
import org.quanticsoftware.automata.facilities.Facility;
import org.quanticsoftware.automata.lib.builder.AutomataBuilder;
import org.quanticsoftware.automata.runtime.AutomataRuntime;
import org.quanticsoftware.automata.runtime.Program;

public class AutomataBase {
	private Context context;
	private Map<String, ScheduledProgram> sprg;
	
	public AutomataBase(Context context) {
		this.context = context;
		this.sprg = new HashMap<>();
	}
	
	private final void build() {
		sprg.forEach((k,v)->v.config.execute(v.pd));
		
		seal();
		
		var typectx = context.typectx();
		var callreq_t = typectx.get("call_request");
		var page_component_t = typectx.get("page_component");
		
		sprg.forEach((k,v)->{
			/*
			 * function: page_get
			 */
			var pageload = v.pd.function("page_get");
			pageload.mtinput(callreq_t);
			pageload.property("call_request", Cauldron.property.CANT_ALLOW_INITIAL);
			pageload.rule(page_component_t, p->{
				var ctx = p.get("call_request");
				var name = ctx.getst("page_name");
				
				var pc = p.output();
				var page = v.pd.getPage(name);
				if (page == null)
					p.fail("'%s' is an invalid page.", name);
				
				pc.set("value", page);
				return pc;
			});
			
			/*
			 * page_load targets
			 */
			for (var p : v.pd.pages()) {
				v.pd.target(
						Concatenate.execute(p, "_page_load"),
						"call_response",
						(t,c)->{
							t.input("call_request", callreq_t);
							
							var test = t.test(Concatenate.execute(p, "_page_test"));
							test.input("call_request").set("page_name", p);
							test.output(o->{
								if (!o.getst("name").equals(p))
									return false;
								
								@SuppressWarnings("unchecked")
								var fields = (Set<String>)o.get("fields");
								return fields != null && !fields.isEmpty();
							});
				});
			}

			for (var key : v.pd.targets())
				v.pd.target(key, context);
		});
	}
	
	public static final AutomataBase instance() {
		var context = new Context();
		var typectx = context.typectx();
		
		var double_t = typectx.get("double_value");
		
		var callreq_t = typectx.define("call_request");
		callreq_t.addst("page_name");
		
		var callresp_t = typectx.define("call_response");
		callresp_t.addst("name");
		callresp_t.add("fields");

		var page_component_t = typectx.define("page_component");
		page_component_t.add("value");
		
		var facility = context.facility("base");
		var add = facility.function("add");
		add.mninput(double_t, "a", "b");
		add.rule(double_t, s->{
			var value = s.output();
			value.set("value", s.getd("a") + s.getd("b"));
			return value;
		});
		add.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		add.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var sub = facility.function("sub");
		sub.mninput(double_t, "a", "b");
		sub.rule(double_t, s->{
			var value = s.output();
			value.set("value", s.getd("a") - s.getd("b"));
			return value;
		});
		sub.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		sub.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var mul = facility.function("mul");
		mul.mninput(double_t, "a", "b");
		mul.rule(double_t, s->{
			var value = s.output();
			value.set("value", s.getd("a") * s.getd("b"));
			return value;
		});
		mul.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		mul.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var div = facility.function("div");
		div.mninput(double_t, "a", "b");
		div.rule(double_t, s->{
			var value = s.output();
			value.set("value", s.getd("a") / s.getd("b"));
			return value;
		});
		div.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		div.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var length = UnitConversion.instance("length");
		length.unit("mm", i->i/1000);
		length.unit("cm", i->i/100);
		length.unit("dm", i->i/10);
		length.unit("m", i->i);
		length.unit("dam", i->i*10);
		length.unit("hm", i->i*100);
		length.unit("km", i->i*1000);

		var time = UnitConversion.instance("time");
		time.unit("ps", i->i/1000000000);
		time.unit("us", i->i/1000000);
		time.unit("ms", i->i/1000);
		time.unit("s", i->i);
		time.unit("m", i->i*60);
		time.unit("h", i->i*3600);
		time.unit("d", i->i*3600*24);

		var force = UnitConversion.instance("force");
		force.unit("N", i->i);
		force.unit("KN", i->i*1000);
		
		var amount_t = typectx.get("amount");
		
		var aadd = facility.function("aadd");
		aadd.mninput(amount_t, "a", "b");
		aadd.rule(amount_t, s->{
			var a = s.get("a");
			var b = s.get("b");
			
			var aunit = a.getst("unit");
			var bunit = b.getst("unit");
			
			if (!UnitConversion.isSameDomain(aunit, bunit))
				s.fail("operators with unmatched unit (%s != %s).", aunit, bunit);
			
			var conversion = UnitConversion.get(aunit);
			var avalue = conversion.execute(bunit, aunit, a.getd("value"));
			
			var value = s.output();
			value.set(avalue + b.getd("value"), bunit);
			
			return value;
		});
		aadd.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		aadd.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var asub = facility.function("asub");
		asub.mninput(amount_t, "a", "b");
		asub.rule(amount_t, s->{
			var a = s.get("a");
			var b = s.get("b");
			
			var aunit = a.getst("unit");
			var bunit = b.getst("unit");
			
			if (!UnitConversion.isSameDomain(aunit, bunit))
				s.fail("operators with unmatched unit (%s != %s).", aunit, bunit);
			
			var conversion = UnitConversion.get(aunit);
			var avalue = conversion.execute(bunit, aunit, a.getd("value"));
			
			var value = s.output();
			value.set(avalue - b.getd("value"), bunit);
			
			return value;
		});
		asub.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		asub.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var amul = facility.function("amul");
		amul.mninput(amount_t, "a", "b");
		amul.rule(amount_t, s->{
			var a = s.get("a");
			var b = s.get("b");
			
			var value = s.output();
			value.set(
					a.getd("value") * b.getd("value"),
					Concatenate.execute(a.getst("unit"), "x", b.getst("unit")));
			
			return value;
		});
		amul.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		amul.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var adiv = facility.function("adiv");
		adiv.mninput(amount_t, "a", "b");
		adiv.rule(amount_t, s->{
			var a = s.get("a");
			var b = s.get("b");
			
			var value = s.output();
			value.set(
					a.getd("value") / b.getd("value"),
					Concatenate.execute(a.getst("unit"), "/", b.getst("unit")));
			
			return value;
		});
		adiv.property("a", Cauldron.property.CANT_ALLOW_INITIAL);
		adiv.property("b", Cauldron.property.CANT_ALLOW_INITIAL);
		
		var string_t = typectx.get("string_value");
		
		var unitconv = facility.function("unit_conversion");
		unitconv.mninput(string_t, "target_unit");
		unitconv.mninput(amount_t, "value");
		unitconv.rule(amount_t, s->{
			var tunit = s.getst("target_unit");
			var amount = s.get("value");
			var sunit = amount.getst("unit");
			var conversion = UnitConversion.get(tunit);
			var cvalue = conversion.execute(tunit, sunit, amount.getd("value"));
			
			var tvalue = s.output();
			tvalue.set(cvalue, tunit);
			
			return tvalue;
		});
		unitconv.property("target_unit", Cauldron.property.CANT_ALLOW_INITIAL);
		unitconv.property("value", Cauldron.property.CANT_ALLOW_INITIAL);

		
		/*
		 * function: page_build
		 */
		var pagebuild = facility.function("page_build");
		pagebuild.mninput(page_component_t, "page");
		pagebuild.property("page", Cauldron.property.CANT_ALLOW_INITIAL);
		pagebuild.rule(callresp_t, p->{
			var pc = (PageComponent)p.get("page").get("value");
			
			var fields = new LinkedHashSet<String>();
			for (var key : pc.items())
				fields.add(Concatenate.execute(
						key,
						"(",
						pc.get(key).type().name(),
						")")
				);
					
			var resp = p.output();
			resp.set("name", pc.name());
			resp.set("fields", fields);
			return resp;
		});
		
		return new AutomataBase(context);
	}
	
	private static final void load_functions(Facility facility, DataType type) {
		if (type.isPrimitive())
			return;
		
		var fncname = type.name();
		var load = facility.function(Concatenate.execute(fncname, "_load"));
		load.mninput(type, "input");
		load.rule(type, s->s.get("input"));
		load.property(Cauldron.property.INPUT_PARAMETER_ONLY);
	}
	
	private static final void objectInstance(Facility facility, DataType type) {
		if (type.isPrimitive())
			return;
		
		var fname = Concatenate.execute(type.name(), "_instance");
		var function = facility.function(fname);
		function.rule(type, s->s.output());
		function.property(Cauldron.property.RETURNS_INITIAL);
	}
	
	private static final void objectSet(
			TypeContext typectx,
			Facility facility,
			DataType type) {
		if (type.isPrimitive() || type.isBoxed())
			return;
		
		var fname = Concatenate.execute(type.name(), "_object_set");
		var function = facility.function(fname);
		function.mninput(type, "object");
		function.property("object", Cauldron.property.UPDATEABLE);
		
		for (var key : type.items()) {
			var atype = type.get(key);
			var atname = atype.name();
			
			var btype = typectx.getBoxedFor(atname);
			if (btype != null)
				atype = typectx.get(btype);
			
			var iname = Concatenate.execute("_", key);
			function.mninput(atype, iname);
			function.property(iname, Cauldron.property.CANT_ALLOW_INITIAL);
		}
		
		function.rule(type, s->{
			var input = s.get("object");
			for (var key : type.items()) {
				var atype = type.get(key);
				var atname = atype.name();
				
				var btype = typectx.getBoxedFor(atname);
				if (btype != null)
					atype = typectx.get(btype);
				
				var aname = Concatenate.execute("_", key);
				var value = atype.isBoxed()? s.getv(aname) : s.get(aname);
				input.set(key, value);
			}
			return input;
		});
		function.property(Cauldron.property.CANT_STACK_SAME);
		function.property(Cauldron.property.CANT_UPDATE_INPUT);
	}
	
	public final ProgramDefinition program(String name, ProgramConfig config) {
		var pd = new ProgramDefinition(context.facility(name), context.typectx());
		sprg.put(name, new ScheduledProgram(pd, config));
		return pd;
	}
	
	private static final void referenceSetFunctions(
			Facility facility,
			DataType type,
			String attribute) {
		if (type.isBoxed() || !type.isReference(attribute))
			return;
		
		var fname = Concatenate.execute(
				type.name(), "_",
				attribute, "_reference_set");
		var function = facility.function(fname);
		function.mninput(type, "target");
		function.mninput(type.get(attribute), "value");
		function.rule(type, s->{
			var input = s.get("target");
			input.set(attribute, s.get("value"));
			return input;
		});
	}
	
	public final DataObject run(
			String prgname,
			String trgname,
			ProgramInput input) throws Exception {
		var prg = sprg.get(prgname);
		if (prg.programs == null) {
			build();
			prg.programs = AutomataBuilder.execute(context);
		}
		
		var target = context.target(trgname);
		var datactx = new DataContext();
		var config = new ProgramInputConfig(target, datactx);
		
		input.execute(config);
			
		return new AutomataRuntime().execute(
				context,
				prg.programs.get(trgname),
				datactx);
	}
	
	private final void seal() {
		var typectx = context.typectx();
		typectx.seal();
		
		var facility = context.getFacility("base");
		
		for (var tkey : typectx.types()) {
			var type = typectx.get(tkey);
			load_functions(facility, type);
			objectInstance(facility, type);
			objectSet(typectx, facility, type);
			
			for (var ikey : type.items()) {
				valueGetFunctions(typectx, facility, type, ikey);
				valueSetFunctions(typectx, facility, type, ikey);
				referenceSetFunctions(facility, type, ikey);
			}
		}
	}
	
	private static final void valueGetFunctions(
			TypeContext typectx,
			Facility facility,
			DataType type,
			String attribute) {
		var atype = type.get(attribute);
		if (type.isBoxed() || !atype.isPrimitive())
			return;
		
		var fname = Concatenate.execute(type.name(), "_", attribute, "_value_get");
		var function = facility.function(fname);
		atype = typectx.get(typectx.getBoxedFor(atype.name()));
		
		function.mninput(type, "input");
		function.rule(atype, s->{
			var output = s.output();
			output.set("value", s.get("input").get(attribute));
			return output;
		});
		function.property("input", Cauldron.property.CANT_ALLOW_INITIAL);
	}
	
	private static final void valueSetFunctions(
			TypeContext typectx,
			Facility facility,
			DataType type,
			String attribute) {
		var atype = type.get(attribute);
		if (type.isBoxed() || type.isReference(attribute) || !atype.isPrimitive())
			return;

		var fname = Concatenate.execute(type.name(), "_", attribute, "_value_set");
		var function = facility.function(fname);
		atype = typectx.get(typectx.getBoxedFor(atype.name()));
		
		function.mninput(type, "target");
		function.mninput(atype, "value");
		function.property("target", Cauldron.property.UPDATEABLE);
		function.rule(type, s->{
			var input = s.get("target");
			input.set(attribute, s.getv("value"));
			return input;
		});
		function.property(Cauldron.property.CANT_STACK_SAME);
		function.property(Cauldron.property.CANT_UPDATE_INPUT);
		function.property("value", Cauldron.property.CANT_ALLOW_INITIAL);
	}

}

class ScheduledProgram {
	public ProgramDefinition pd;
	public ProgramConfig config;
	public Map<String, Program> programs;
	
	public ScheduledProgram(ProgramDefinition pd, ProgramConfig config) {
		this.pd = pd;
		this.config = config;
	}
}
