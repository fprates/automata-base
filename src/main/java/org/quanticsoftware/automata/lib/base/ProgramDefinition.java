package org.quanticsoftware.automata.lib.base;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.quanticsoftware.automata.core.Concatenate;
import org.quanticsoftware.automata.core.Context;
import org.quanticsoftware.automata.core.DataType;
import org.quanticsoftware.automata.core.TypeContext;
import org.quanticsoftware.automata.facilities.Facility;
import org.quanticsoftware.automata.facilities.Function;

public class ProgramDefinition {
	private Facility facility;
	private TypeContext typectx;
	private DataType program_t, pages_t, pctypes_t;
	private Map<String, PageComponent> pages;
	private String pname;
	private Map<String, TargetDefinition> targets;
	
	public ProgramDefinition(Facility facility, TypeContext typectx) {
		this.facility = facility;
		this.typectx = typectx;
		pages = new HashMap<>();

		pctypes_t = typectx.define("pctypes");
		for (var key : new String[] {
				"button",
				"custom",
				"dataform",
				"section",
				"table",
				"tab",
				"tabs"}) {
			var pctype = typectx.define(key);

			pctypes_t.add(pctype, key);
		}
		
		pname = facility.name();
		pages_t = typectx.define(Concatenate.execute("pages_", pname));

		var name = Concatenate.execute("program_", pname);
		program_t = typectx.define(name);
		program_t.add(pages_t, "pages");
		
		targets = new HashMap<>();
	}
	
	public final Function function(String name) {
		return facility.function(name);
	}
	
	public final PageComponent getPage(String name) {
		return pages.get(name);
	}
	
	public final PageComponent page(String name) {
		var pagename = Concatenate.execute("page_", pname, "_", name);
		var page_t = typectx.define(pagename);
		
		pages_t.add(page_t, name);
		
		var page = new PageComponent(name, null, null, page_t, pctypes_t);
		pages.put(name, page);
		return page;
	}
	
	public final Set<String> pages() {
		return pages.keySet();
	}
	
	public final void target(String name, String type, TargetConfig config) {
		targets.put(name, new TargetDefinition(type, config));
	}
	
	public final void target(String name, Context context) {
		var td = targets.get(name);
		
		var target = context.target(name);
		target.output(typectx.get(td.type));
		
		td.config.execute(target, context);
	}
	
	public final Set<String> targets() {
		return targets.keySet();
	}
	
	public final DataType type(String name) {
		return typectx.define(name);
	}
}

class TargetDefinition {
	public String type;
	public TargetConfig config;
	
	public TargetDefinition(String type, TargetConfig config) {
		this.type = type;
		this.config = config;
	}
}

