package org.quanticsoftware.automata.lib.base;

public interface ProgramInput {
	
	public abstract void execute(ProgramInputConfig config);
	
}
