package org.quanticsoftware.automata.lib.base;

public interface UnitConversionRule {
	
	public abstract double convert(double input);
	
}
